import 'package:get/get.dart';
import 'package:wasty/app/modules/welecome/controllers/welecome_controller.dart';

class WelecomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WelcomeController>(() {
      return WelcomeController();
    });
  }
}
